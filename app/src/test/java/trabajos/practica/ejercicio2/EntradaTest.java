package trabajos.practica.ejercicio2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class EntradaTest {

    // TESTS de fecha
    @Test
    public void testFechaVenta() {
        Entrada venta = new Entrada(02, 02, "Jhon Wick", 1200, LocalDate.of(2023, 5, 2),
        LocalDate.of(2023, 5, 2),LocalTime.now(), "Apto para ninguno público");
        // Creo el objeto fecha que esperaré
        LocalDate fecha = LocalDate.of(2022, 5, 1);
        // Asigno ese objeto al atributo fecha_venta en mi objeto ENTRADA
        venta.setFecha_venta(fecha);
        
        // Compruebo si ambos objetos coinciden en valor
        assertEquals(fecha, venta.getFecha_venta());
    }

    @Test
    public void testFechaVentaNull() {
        // Creo mi objeto
        //Entrada venta = new Entrada();

        Entrada venta = new Entrada(02, 02, "Jhon Wick", 1200, LocalDate.of(2023, 5, 2),
        LocalDate.of(2023, 5, 2),LocalTime.now(), "Apto para ninguno público");

        // assertThrows chequea los throws messages que arrojan
        // cuando aparecen excepciones. En este caso, se espera que arroje
        // una excepción de tipo NO NULL (NullPointerException), que se llama
        // cuando un null no es permitido.
        // con la arrow nosotros

        // AssertThrows tiene dos argumentos:
        // 1) La excepción esperada
        // 2) () -> venta.setFecha_venta(null) una función lambda, que nos es más
        // que una función anónima en javascript
        assertThrows(NullPointerException.class, () -> venta.setFecha_venta(null));
    }

    @Test
    public void testFechaVentaFutureDate() {
        Entrada venta = new Entrada(02, 02, "Jhon Wick", 1200, LocalDate.of(2023, 5, 2),
        LocalDate.of(2023, 5, 2),LocalTime.now(), "Apto para ninguno público");
        LocalDate fecha = LocalDate.now().plusDays(1);
        assertThrows(IllegalArgumentException.class, () -> venta.setFecha_venta(fecha));
    }


    @Test
    public void testGetClasificacion_peli() {

    }

    @Test
    public void testGetFecha_funcion() {

    }

    @Test
    public void testGetFecha_venta() {

    }

    @Test
    public void testGetHora_funcion() {

    }

    @Test
    public void testGetNombre_pelicula() {

    }

    @Test
    public void testGetNro_ticket() {

    }

    @Test
    public void testGetNum_funcion() {

    }

    @Test
    public void testGetPrecio() {

    }

    @Test
    public void testSetClasificacion_peli() {

    }

    @Test
    public void testSetFecha_funcion() {

    }

    @Test
    public void testSetFecha_venta() {

    }

    @Test
    public void testSetHora_funcion() {

    }

    @Test
    public void testSetNombre_pelicula() {

    }

    @Test
    public void testSetNro_ticket() {

    }

    @Test
    public void testSetNum_funcion() {

    }

    @Test
    public void testSetPrecio() {

    }
}
