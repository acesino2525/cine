package trabajos.practica.ejercicio2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class PeliculaTest {
	@Test
	public void testGetActores() {

	}

	@Test
	public void testGetDirector() {

	}

	@Test
	public void testGetDuracion() {

	}

	@Test
	public void testGetEdad_minima() {

	}

	@Test
	public void testGetGenero() {

	}

	@Test
	public void testGetTitulo() {

	}

	@Test
	public void testSetActores() {
        // creamos una lista vacía
        ArrayList<Actor> list = new ArrayList<>();
        // guardamos constructores para su posterior uso:
        Actor keanu = new Actor("Keanu", "Reevs");
        Actor ian = new Actor("McShane", "Ian");
        Actor scott = new Actor("Scott", "Adkins");
        // agregamos elementos
        list.add(keanu);
        list.add(ian);
        list.add(scott);
        // TESTS
        // Controlamos el tamaño del array
        assertEquals(3, list.size());

        // Controlamos que lo creado exista en el array
        assertTrue(list.contains(keanu));
        assertTrue(list.contains(ian));
        assertTrue(list.contains(scott));
	}

	@Test
	public void testSetDirector() {

	}

	@Test
	public void testSetDuracion() {

	}

	@Test
	public void testSetEdad_minima() {

	}

	@Test
	public void testSetGenero() {

	}

	@Test
	public void testSetTitulo() {

	}
}
