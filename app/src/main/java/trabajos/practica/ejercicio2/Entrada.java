package trabajos.practica.ejercicio2;

import java.time.LocalDate;

public class Entrada implements Item {

    private int nro_ticket;
    private LocalDate fecha_venta;

    // Vamos a guardar la película a la que está asociada
    private Pelicula pelicula;
    // Vamos a guardar la sala a la que está asociado
    private Sala sala;
    // Vamos a guardar la función para comparar con la sala luego
    private Funcion funcion;

    // Creado para el propósito de testing
    /*public Entrada(){

    }*/
    public Entrada(int nro_ticket, LocalDate fecha_venta, Pelicula pelicula, Sala sala,
            Funcion funcion) {
        this.nro_ticket = nro_ticket;
        this.fecha_venta = fecha_venta;
        this.sala = sala;
        this.funcion = funcion;
    }

    

    public Sala getSala() {
        return sala;
    }



    public void setSala(Sala sala) {
        this.sala = sala;
    }



    public Funcion getFuncion() {
        return funcion;
    }



    public void setFuncion(Funcion funcion) {
        this.funcion = funcion;
    }



    public int getNro_ticket() {
        return nro_ticket;
    }

    public void setNro_ticket(int nro_ticket) {
        this.nro_ticket = nro_ticket;
    }

    public LocalDate getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(LocalDate fecha_venta) {

        if (fecha_venta == null) {
            throw new NullPointerException("Fecha de venta no puede ser nula");
        }

        // Puede venderse a futuro, pero para testear a ver
        if (fecha_venta.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Fecha de venta no puede ser en el futuro");
        }

        this.fecha_venta = fecha_venta;
    }



    public Pelicula getPelicula() {
        return pelicula;
    }



    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

}
