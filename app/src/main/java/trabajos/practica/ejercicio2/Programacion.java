package trabajos.practica.ejercicio2;

public class Programacion {
    private String fecha_ini;
    private String fecha_fin;
    // Cada programación puede tener más de una función
    private String funciones[];

/*
 * Las programaciones tienen una fecha
 */
public Programacion(String fecha_fin, String fecha_ini, String funciones[]){

    this.fecha_fin = fecha_fin;
    this.fecha_ini = fecha_ini;
    this.funciones = funciones;

 }

public String getFecha_ini() {
    return fecha_ini;
}

public void setFecha_ini(String fecha_ini) {
    this.fecha_ini = fecha_ini;
}

public String getFecha_fin() {
    return fecha_fin;
}

public void setFecha_fin(String fecha_fin) {
    this.fecha_fin = fecha_fin;
}

public String[] getFunciones() {
    return funciones;
}

public void setFunciones(String[] funciones) {
    this.funciones = funciones;
}

}
