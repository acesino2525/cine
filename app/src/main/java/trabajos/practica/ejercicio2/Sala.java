package trabajos.practica.ejercicio2;

import java.util.ArrayList;
import java.util.List;

public class Sala {
    private int idSala;
    private boolean completa;
    // Una sala tiene 80 butacas, la iremos llenando con el front. Porque pueden
    // ser distintas disposiciones las de las salas, por ende distintas filas
    // y debemos utilizar la misma clase para todas
    private List<Butaca> butacas = new ArrayList<Butaca>();

    public Sala (int idSala, boolean completa, List<Butaca> butacas){
        this.idSala = idSala;
        this.completa = completa;
        this.butacas = butacas;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    

    public void disponible(){

    }

    public boolean isCompleta() {
        return completa;
    }

    public void setCompleta(boolean completa) {
        this.completa = completa;
    }



    public List<Butaca> getButacas() {
        return butacas;
    }



    public void setButacas(List<Butaca> butacas) {
        this.butacas = butacas;
    }

}
