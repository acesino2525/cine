package trabajos.practica.ejercicio2;

public enum EnumFormasDePago {
    EFECTIVO,
    TARJETA_CREDITO,
    TARJETA_DEBITO,
    TRANSFERENCIA
}
