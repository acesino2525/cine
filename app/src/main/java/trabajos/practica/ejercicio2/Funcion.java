package trabajos.practica.ejercicio2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Funcion {
    private LocalDate fecha;
    private int hora;

    // Salas a las que se le asigna esta función
    private List<Sala> salas = new ArrayList<Sala>();

    public Funcion(LocalDate fecha, int hora, List<Sala> salas){

        this.fecha = fecha;
        this.hora = hora;
        this.salas = salas;

    }

    

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }



    public List<Sala> getSalas() {
        return salas;
    }



    public void setSalas(List<Sala> salas) {
        this.salas = salas;
    }

    

}