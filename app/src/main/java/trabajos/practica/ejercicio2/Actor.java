package trabajos.practica.ejercicio2;

public class Actor {
    private String apellido;
    private String nombre;
    private String id;

    public Actor(String apellido, String nombre, String id) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
