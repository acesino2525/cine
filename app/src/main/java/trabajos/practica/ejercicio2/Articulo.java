package trabajos.practica.ejercicio2;

import java.math.BigDecimal;

public class Articulo implements Item{
    // idItem, nombre item, precio item
    private int idItem;
    private String nombreItem;
    private BigDecimal precio;

    public Articulo(int idItem, String nombreItem, BigDecimal precio) {
        this.idItem = idItem;
        this.nombreItem = nombreItem;
        this.precio = precio;
    }
    
    public int getIdItem() {
        return idItem;
    }
    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }
    public String getNombreItem() {
        return nombreItem;
    }
    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }
    public BigDecimal getPrecio() {
        return precio;
    }
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    
    
}
