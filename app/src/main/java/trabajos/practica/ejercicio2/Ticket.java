package trabajos.practica.ejercicio2;

import java.util.ArrayList;
import java.util.List;

public class Ticket {
    // idFactura
    // Quien compra
    // Items comprados (los items van a ser un tipo de dato que engloba a entrada)

    private int idFactura;
    private Comprador comprador;
    private List<Item> items = new ArrayList<>();

    public Ticket(int idFactura, Comprador comprador, List<Item> items) {
        this.idFactura = idFactura;
        this.comprador = comprador;
        this.items = items;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    
    
}
