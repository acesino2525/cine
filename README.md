# CRUD ACTORES

![Texto alternativo](https://gitlab.com/acesino2525/cine/-/raw/main/README/CRUD%20ACTORES.png)

__CREATE:__
Creas cargando los textfields e inmediatamente se visualiza en la tabla

__READ:__
Al poner el id en el campo ID y dar enter se rellenan los campos con el resultado que coincida con la búsqueda
La tabla te permite leer los respectivos datos

__UPDATE:__
Al performar la búsqueda también puedes editar aunque cambies el id, porque se busca por posición en el array

__DELETE:__
Eliminas tras hacer una búsqueda del ID que deseas eliminar